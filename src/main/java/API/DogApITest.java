package API;

//import com.fasterxml.jackson.databind.JsonNode;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;




import static API.Base.BASEURL;
import static com.jayway.restassured.RestAssured.given;

public class DogApITest {

   public DogApITest(){
       RestAssured.baseURI=BASEURL;
   }



public Response performCalls(String url)throws Throwable{


       return given()
               .contentType(ContentType.JSON)
               .when()
               .get(url)
               .then()
               .assertThat()
               .statusCode(200)
               .and()
               .contentType(ContentType.JSON).extract().response();
}

}
