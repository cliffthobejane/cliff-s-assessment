package API;

public class PerformAllCalls {
   private  ApiData apiData;
    DogApITest dogApITest=new DogApITest();
    public PerformAllCalls(){
        apiData =new ApiData("https://dog.ceo/api/breeds/list/all","https://dog.ceo/api/breed/retriever/list","https://dog.ceo/api/breed/retriever/golden/images/random");

    }

   public void displayAlldogsBreed()throws Throwable{
       dogApITest.performCalls(apiData.getList_of_all_dog_breeds()).prettyPrint();


       }
       public Boolean verify_retriever_breed_is_within_the_list()throws Throwable{

       Boolean value=true;

       String response=dogApITest.performCalls(apiData.getList_of_all_dog_breeds()).asString();
          if(response.contains("retrieve")){
              System.out.println(value+" "+"Retrieve is within the list");

              return true;
          }
          return false;
       }
       public void listSubBreed()throws Throwable{


           dogApITest.performCalls(apiData.getList_of_sub_breeds_for_retriever()).prettyPrint();

       }
       public void imageLinkGolden()throws Throwable{

           dogApITest.performCalls(apiData.getImage_link_for_the_sub_breed_golden()).prettyPrint();

       }
}
