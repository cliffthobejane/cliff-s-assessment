package API;

public class ApiData {
    private String list_of_all_dog_breeds;
    private String list_of_sub_breeds_for_retriever;
    private String image_link_for_the_sub_breed_golden;

    public ApiData(String list_of_all_dog_breeds,String list_of_sub_breeds_for_retriever,String image_link_for_the_sub_breed_golden)
    {
        this.list_of_all_dog_breeds=list_of_all_dog_breeds;
        this.list_of_sub_breeds_for_retriever=list_of_sub_breeds_for_retriever;
        this.image_link_for_the_sub_breed_golden=image_link_for_the_sub_breed_golden;
    }

    public String getList_of_all_dog_breeds() {
        return list_of_all_dog_breeds;
    }



    public String getList_of_sub_breeds_for_retriever() {
        return list_of_sub_breeds_for_retriever;
    }



    public String getImage_link_for_the_sub_breed_golden() {
        return image_link_for_the_sub_breed_golden;
    }


}
